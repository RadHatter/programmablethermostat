package com.programmablethermostat.main;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author aarjs
 */
public class ThermSched { 
    
    private List<DailySched> weeklySched;
    private List<DailySched> holidaySched;
    
    public ThermSched() {
        weeklySched = new ArrayList<>();
        holidaySched = new ArrayList<>();
        for (int day = 0; day < 11; day++) {
            switch(day) {
                case 0:
                    weeklySched.add(new DailySched("Sun"));
                    break;
                case 1:
                    weeklySched.add(new DailySched("Mon"));
                    break;
                case 2:
                    weeklySched.add(new DailySched("Tues"));
                    break;
                case 3:
                    weeklySched.add(new DailySched("Wed"));
                    break;
                case 4:
                    weeklySched.add(new DailySched("Thurs"));
                    break;
                case 5:
                    weeklySched.add(new DailySched("Fri"));
                    break;
                case 6:
                    weeklySched.add(new DailySched("Sat"));
                    break;
                case 7:
                    weeklySched.add(new DailySched("Weekday"));
                    break;
                case 8:
                    weeklySched.add(new DailySched("Weekend"));
                    break;
                case 9:
                    weeklySched.add(new DailySched("Every Day"));
                    break;
                case 10:
                    holidaySched.add(new DailySched("Holiday"));
                    break;
            }
        }
    }
    
    public void resetSchedule(int schedIndex) {
        String day = "";
        switch(schedIndex) {
            case 0:
                day = "Sun";
                break;
            case 1:
                day = "Mon";
                break;
            case 2:
                day = "Tues";
                break;
            case 3:
                day = "Wed";
                break; 
            case 4:
                day = "Thurs";
                break;
            case 5:
                day = "Fri";
                break;
            case 6:
                day = "Saturday";
                break;
            case 7:
                day = "Weekday";
                break;
            case 8:
                day = "Weekend";
                break;
            case 9:
                day = "Every Day";
                break;
        }
        weeklySched.set(schedIndex, new DailySched(day));
        switch (schedIndex) {
            case 7:
                syncWeekDays();
                break;
            case 8:
                syncWeekendDays();
                break;
            case 9:
                syncEveryDay();
                break;
            default:
                break;
        }
    }
    
    public void desyncWeekDays() {
        weeklySched.set(7, new DailySched("Weekday"));
    }
    
    public void syncWeekDays() {
        DailySched mo = weeklySched.get(1);
        DailySched tu = weeklySched.get(2);
        DailySched we = weeklySched.get(3);
        DailySched th = weeklySched.get(4);
        DailySched fr = weeklySched.get(5);
        DailySched weekday = weeklySched.get(7);
        
        mo.morningHvacMode = tu.morningHvacMode = we.morningHvacMode = th.morningHvacMode = fr.morningHvacMode = weekday.morningHvacMode;
        mo.morningPeriod = tu.morningPeriod = we.morningPeriod = th.morningPeriod = fr.morningPeriod = weekday.morningPeriod;
        mo.morningStart = tu.morningStart = we.morningStart = th.morningStart = fr.morningStart = weekday.morningStart;
        mo.morningTarget = tu.morningTarget = we.morningTarget = th.morningTarget = fr.morningTarget = weekday.morningTarget;
        
        mo.eveningHvacMode = tu.eveningHvacMode = we.eveningHvacMode = th.eveningHvacMode = fr.eveningHvacMode = weekday.eveningHvacMode;
        mo.eveningPeriod = tu.eveningPeriod = we.eveningPeriod = th.eveningPeriod = fr.eveningPeriod = weekday.eveningPeriod;
        mo.eveningStart = tu.eveningStart = we.eveningStart = th.eveningStart = fr.eveningStart = weekday.eveningStart;
        mo.eveningTarget = tu.eveningTarget = we.eveningTarget = th.eveningTarget = fr.eveningTarget = weekday.eveningTarget;
        
        mo.nightHvacMode = tu.nightHvacMode = we.nightHvacMode = th.nightHvacMode = fr.nightHvacMode = weekday.nightHvacMode;
        mo.nightPeriod = tu.nightPeriod = we.nightPeriod = th.nightPeriod = fr.nightPeriod = weekday.nightPeriod;
        mo.nightStart = tu.nightStart = we.nightStart = th.nightStart = fr.nightStart = weekday.nightStart;
        mo.nightTarget = tu.nightTarget = we.nightTarget = th.nightTarget = fr.nightTarget = weekday.nightTarget;
    }
    
    public void desyncWeekendDays() {
        weeklySched.set(8, new DailySched("Weekend"));
    }
    
    public void syncWeekendDays() {
        DailySched sunday = weeklySched.get(0);
        DailySched saturday = weeklySched.get(6);
        DailySched weekend = weeklySched.get(8);
        
        sunday.morningHvacMode = saturday.morningHvacMode = weekend.morningHvacMode;
        sunday.morningPeriod = saturday.morningPeriod = weekend.morningPeriod;
        sunday.morningStart = saturday.morningStart = weekend.morningStart;
        sunday.morningTarget = saturday.morningTarget = weekend.morningTarget;
        
        sunday.eveningHvacMode = saturday.eveningHvacMode = weekend.eveningHvacMode;
        sunday.eveningPeriod = saturday.eveningPeriod = weekend.eveningPeriod;
        sunday.eveningStart = saturday.eveningStart = weekend.eveningStart;
        sunday.eveningTarget = saturday.eveningTarget = weekend.eveningTarget;
        
        sunday.nightHvacMode = saturday.nightHvacMode = weekend.nightHvacMode;
        sunday.nightPeriod = saturday.nightPeriod = weekend.nightPeriod;
        sunday.nightStart = saturday.nightStart = weekend.nightStart;
        sunday.nightTarget = saturday.nightTarget = weekend.nightTarget;
    }
    
    public void desyncEveryDay() {
        weeklySched.set(7, new DailySched("Weekday"));
        weeklySched.set(8, new DailySched("Weekend"));
        weeklySched.set(9, new DailySched("Every Day"));
    }
    
    public void syncEveryDay() {
        DailySched everyDay = weeklySched.get(9);
        for(int day = 0; day < 9; day++) {
            DailySched thisDay = weeklySched.get(day);
            
            thisDay.morningHvacMode = everyDay.morningHvacMode;
            thisDay.morningPeriod = everyDay.morningPeriod;
            thisDay.morningStart = everyDay.morningStart;
            thisDay.morningTarget = everyDay.morningTarget;
        
            thisDay.eveningHvacMode = everyDay.eveningHvacMode;
            thisDay.eveningPeriod = everyDay.eveningPeriod;
            thisDay.eveningStart = everyDay.eveningStart;
            thisDay.eveningTarget = everyDay.eveningTarget;
        
            thisDay.nightHvacMode = everyDay.nightHvacMode;
            thisDay.nightPeriod = everyDay.nightPeriod;
            thisDay.nightStart = everyDay.nightStart;
            thisDay.nightTarget = everyDay.nightTarget;
        }        
    }
    
    public void addHoliday(int month, int day, int year) {
        DailySched newHoliday = new DailySched("Holiday" + String.valueOf(holidaySched.size()));
        newHoliday.addHoliday(month, day, year);
        holidaySched.add(newHoliday);
    }
    
    /**
     *
     * @param dayIndex
     * @return
     */
    public DailySched getDailySched(int dayIndex) {
        return weeklySched.get(dayIndex);
    }
    
    public DailySched getHolidaySched(int holidayIndex) {
        return holidaySched.get(holidayIndex);
    }
    
    public void delHoliday(int holidayIndex) {
        if(holidayIndex > 0) {
            holidaySched.remove(holidayIndex);
        }
        if(holidaySched.size() > 1)
        for(int holiday = 1; holiday < holidaySched.size(); holiday++) {
            holidaySched.get(holiday).day = "Holiday" + String.valueOf(holiday);
        }
    }
    
    public int getHolidayCount() {
        return holidaySched.size() - 1;
    }
    
    public class DailySched {
        private String day;        
        private int morningStart;
        private char morningPeriod;
        private int morningTarget;
        private char morningHvacMode;
        private int eveningStart;
        private char eveningPeriod;
        private int eveningTarget;
        private char eveningHvacMode;
        private int nightStart;
        private char nightPeriod;
        private int nightTarget;
        private char nightHvacMode;
        private HolidayDate holidayDate;
        
        public DailySched(String day) {
            this.day = day;
            morningStart = eveningStart = nightStart = 1200;
            morningPeriod = eveningPeriod = nightPeriod = 'A';
            morningTarget = 65;
            eveningTarget = 65;
            nightTarget = 65;
            morningHvacMode = eveningHvacMode = nightHvacMode = 'A';
        }
        
        public String getDay() {
            return day;
        }
        
        public void setDay(String day) {
            this.day = day;
        }
        
        public void increaseMorningHour() {
            morningStart += 100;
            if(morningStart >= 1300) {
                morningStart -= 1200;
            }
        }
        
        public void increaseMorningMinute() {
            morningStart += 5;
            if(morningStart % 100 > 59) {
                morningStart -= 60;
            }
        }
        
        public int getMorningStart() {
            return morningStart;
        }
        
        public void increaseMorningTarget() {
            if(morningTarget < 90) {
                morningTarget++;
            }
        }
        
        public void decreaseMorningTarget() {
            if(morningTarget > 50) {
                morningTarget--;
            }
        }
        
        public void setMorningTarget(int morningTarget) {
            this.morningTarget = morningTarget;
        }
        
        public int getMorningTarget() {
            return morningTarget;
        }
        
        public void toggleMorningPeriod() {
            if(morningPeriod == 'A') {
                morningPeriod = 'P';
            }
            else {
                morningPeriod = 'A';
            }
        }
        
        public char getMorningPeriod() {
            return morningPeriod;
        }
        
        public void prevMorningHvacMode() {
            switch(morningHvacMode) {
                case 'A':
                    morningHvacMode = 'O';
                    break;
                case 'C':
                    morningHvacMode = 'A';
                    break;
                case 'H':
                    morningHvacMode = 'C';
                    break;
                case 'O':
                    morningHvacMode = 'H';
                    break;
            }
        }
        
        public void nextMorningHvacMode() {
            switch(morningHvacMode) {
                case 'A':
                    morningHvacMode = 'C';
                    break;
                case 'C':
                    morningHvacMode = 'H';
                    break;
                case 'H':
                    morningHvacMode = 'O';
                    break;
                case 'O':
                    morningHvacMode = 'A';
                    break;
            }
        }
        
        public void setMorningHvacMode(char morningHvacMode) {
            this.morningHvacMode = morningHvacMode;
        }
        
        public char getMorningHvacMode() {
            return morningHvacMode;
        }
        
        public String getMorningStartString() {
            int hour = morningStart / 100;
            int minute = morningStart % 100;
            
            String minuteValue = "";
            if(minute < 10) {
                minuteValue += "0";
            }
            minuteValue += String.valueOf(minute);
            String returnString = String.valueOf(hour) + ":" + minuteValue + morningPeriod;
            return returnString;
        }
        
        public void increaseEveningHour() {
            eveningStart += 100;
            if(eveningStart >= 1300) {
                eveningStart -= 1200;
            }
        }
        
        public void increaseEveningMinute() {
            eveningStart += 5;
            if(eveningStart % 100 > 59) {
                eveningStart -= 60;
            }
        }
        
        public int getEveningStart() {
            return eveningStart;
        }
        
        public void increaseEveningTarget() {
            if(eveningTarget < 90) {
                eveningTarget++;
            }
        }
        
        public void decreaseEveningTarget() {
            if(eveningTarget > 50) {
                eveningTarget--;
            }
        }
        
        public int getEveningTarget() {
            return eveningTarget;
        }
        
        public void toggleEveningPeriod() {
            if(eveningPeriod == 'A') {
                eveningPeriod = 'P';
            }
            else {
                eveningPeriod = 'A';
            }
        }
        
        public char getEveningPeriod() {
            return eveningPeriod;
        }
        
        public void prevEveningHvacMode() {
            switch(eveningHvacMode) {
                case 'A':
                    eveningHvacMode = 'O';
                    break;
                case 'C':
                    eveningHvacMode = 'A';
                    break;
                case 'H':
                    eveningHvacMode = 'C';
                    break;
                case 'O':
                    eveningHvacMode = 'H';
                    break;
            }
        }
        
        public void nextEveningHvacMode() {
            switch(eveningHvacMode) {
                case 'A':
                    eveningHvacMode = 'C';
                    break;
                case 'C':
                    eveningHvacMode = 'H';
                    break;
                case 'H':
                    eveningHvacMode = 'O';
                    break;
                case 'O':
                    eveningHvacMode = 'A';
                    break;
            }
        }
        
        public void setEveningHvacMode(char eveningHvacMode) {
            this.eveningHvacMode = eveningHvacMode;
        }
        
        public char getEveningHvacMode() {
            return eveningHvacMode;
        }
        
        public String getEveningStartString() {
            int hour = eveningStart / 100;
            int minute = eveningStart % 100;
            String minuteValue = "";
            if(minute < 10) {
                minuteValue += "0";
            }
            minuteValue += String.valueOf(minute);
            return(String.valueOf(hour) + ":" + minuteValue + eveningPeriod);
        }
        
        public void increaseNightHour() {
            nightStart += 100;
            if(nightStart >= 1300) {
                nightStart -= 1200;
            }
        }
        
        public void increaseNightMinute() {
            nightStart += 5;
            if(nightStart % 100 > 59) {
                nightStart -= 60;
            }
        }
        
        public int getNightStart() {
            return nightStart;
        }
        
        public void increaseNightTarget() {
            if(nightTarget < 90) {
                nightTarget++;
            }
        }
        
        public void decreaseNightTarget() {
            if(nightTarget > 50) {
                nightTarget--;
            }
        }
        
        public void setNightTarget(int nightTarget) {
            this.nightTarget = nightTarget;
        }
        
        public int getNightTarget() {
            return nightTarget;
        }
        
        public void toggleNightPeriod() {
            if(nightPeriod == 'A') {
                nightPeriod = 'P';
            }
            else {
                nightPeriod = 'A';
            }
        }
        
        public char getNightPeriod() {
            return nightPeriod;
        }
        
        public void prevNightHvacMode() {
            switch(nightHvacMode) {
                case 'A':
                    nightHvacMode = 'O';
                    break;
                case 'C':
                    nightHvacMode = 'A';
                    break;
                case 'H':
                    nightHvacMode = 'C';
                    break;
                case 'O':
                    nightHvacMode = 'H';
                    break;
            }
        }
        
        public void nextNightHvacMode() {
            switch(nightHvacMode) {
                case 'A':
                    nightHvacMode = 'C';
                    break;
                case 'C':
                    nightHvacMode = 'H';
                    break;
                case 'H':
                    nightHvacMode = 'O';
                    break;
                case 'O':
                    nightHvacMode = 'A';
                    break;
            }
        }
        
        public char getNightHvacMode() {
            return nightHvacMode;
        }
        
        public String getNightStartString() {
            int hour = nightStart / 100;
            int minute = nightStart % 100;
            String minuteValue = "";
            if(minute < 10) {
                minuteValue += "0";
            }
            minuteValue += String.valueOf(minute);
            return(String.valueOf(hour) + ":" + minuteValue + nightPeriod);
        }
        
        public void addHoliday(int month, int day, int year) {
            holidayDate = new HolidayDate();
            holidayDate.holiMonth = month;
            holidayDate.holiDay = day;
            holidayDate.holiYear = year;
        }
        
        public HolidayDate getHoliday() {
            return holidayDate;
        }
        
        public class HolidayDate {
            public int holiDay = Calendar.DAY_OF_MONTH;
            public int holiMonth = Calendar.MONTH;
            public int holiYear = Calendar.YEAR;
        } 
    }   
}