package com.programmablethermostat.main;

import java.time.LocalDateTime;

/**
 * @author aarjs
 */

/*A JSON entity class to save and retrieve the thermostat's status */
public class ThermStatus {
    private long identifier;
    private String name;
    private String thermostatTime;
    private String utcTime;
    private Runtime runtime;
    private Status status;
    private int targetTemp;
    private boolean fanOn = false;
    public char hvacMode = 'A';
    
    //Loads the desired parameters from a saved schedule
    public void loadSchedParams(ThermSched activeSched) {
        LocalDateTime now = LocalDateTime.now();
        ThermSched.DailySched yestSched = null;
        ThermSched.DailySched todaySched = null;
        switch(now.getDayOfWeek()) {
            case SUNDAY:
                yestSched = activeSched.getDailySched(6);
                todaySched = activeSched.getDailySched(0);
                break;
            case MONDAY:
                yestSched = activeSched.getDailySched(0);
                todaySched = activeSched.getDailySched(1);
                break;
            case TUESDAY:
                yestSched = activeSched.getDailySched(1);
                todaySched = activeSched.getDailySched(2);
                break;
            case WEDNESDAY:
                yestSched = activeSched.getDailySched(2);
                todaySched = activeSched.getDailySched(3);
                break;                
            case THURSDAY:
                yestSched = activeSched.getDailySched(3);
                todaySched = activeSched.getDailySched(4);
                break;
            case FRIDAY:
                yestSched = activeSched.getDailySched(4);
                todaySched = activeSched.getDailySched(5);
                break;
            case SATURDAY:
                yestSched = activeSched.getDailySched(5);
                todaySched = activeSched.getDailySched(6);
                break;                
        }
        
        //Determines AM or PM based on the DateTime stamp
        char nowPeriod = ' ';
        if(now.getHour() < 12) {
            nowPeriod = 'A';
        } else {
            nowPeriod = 'P';
        }
        
        //HAS THE SAVED MORNING PERIOD PASSED?
        if(todaySched.getMorningPeriod() == 'A' && nowPeriod == 'A') {
            if(todaySched.getMorningStart() / 100 < now.getHour()) {
                if(todaySched.getNightStart() % 100 <= now.getMinute()) {
                    targetTemp = todaySched.getMorningTarget();
                    hvacMode = todaySched.getMorningHvacMode();
                } else {
                    targetTemp = yestSched.getNightTarget();
                    hvacMode = yestSched.getNightHvacMode();
                }
            }
            else {
                targetTemp = yestSched.getNightTarget();
                hvacMode = yestSched.getNightHvacMode();
            }
        }
        else if (todaySched.getMorningPeriod() == 'P' && nowPeriod == 'P') {
            if(todaySched.getMorningStart() / 100 < now.getHour() - 12) {
                if(todaySched.getNightStart() % 100 <= now.getMinute()) {
                    targetTemp = todaySched.getMorningTarget();
                    hvacMode = yestSched.getMorningHvacMode();
                } else {
                    targetTemp = yestSched.getNightTarget();
                    hvacMode = yestSched.getNightHvacMode();
                }
            }
            else {
                targetTemp = yestSched.getNightTarget();
                hvacMode = yestSched.getNightHvacMode();
            }
        }
        //HAS THE SAVED EVENING PERIOD PASSED?
        if(todaySched.getEveningPeriod() == 'A' && nowPeriod == 'A') {
            if(todaySched.getEveningStart() / 100 < now.getHour()) {
                if(todaySched.getNightStart() % 100 <= now.getMinute()) {
                    targetTemp = todaySched.getEveningTarget();
                    hvacMode = todaySched.getEveningHvacMode();
                } else {
                    targetTemp = todaySched.getMorningTarget();
                    hvacMode = todaySched.getMorningHvacMode();
                }
            }
            else {
                targetTemp = todaySched.getMorningTarget();
                hvacMode = todaySched.getMorningHvacMode();
            }
        }
        else if (todaySched.getEveningPeriod() == 'P') {
            if (nowPeriod == 'P') {
                if(todaySched.getEveningStart() / 100 < now.getHour() - 12) {
                    if(todaySched.getNightStart() % 100 <= now.getMinute()) {
                    targetTemp = todaySched.getEveningTarget();
                    hvacMode = todaySched.getEveningHvacMode();
                } else {
                    targetTemp = todaySched.getMorningTarget();
                    hvacMode = todaySched.getMorningHvacMode();
                }
                }
                else {
                    targetTemp = todaySched.getMorningTarget();
                    hvacMode = todaySched.getMorningHvacMode();
                }
            }
        }
        //HAS THE SAVED NIGHT PERIOD PASSED?
        if(todaySched.getNightPeriod() == 'A' && nowPeriod == 'A') {
            if(todaySched.getNightStart() / 100 <= now.getHour()) {
                if(todaySched.getNightStart() % 100 <= now.getMinute()) {
                    targetTemp = todaySched.getNightTarget();
                    hvacMode = todaySched.getEveningHvacMode();
                } else {
                    targetTemp = todaySched.getEveningTarget();
                    hvacMode = todaySched.getEveningHvacMode();
                }
            }
            else {
                targetTemp = todaySched.getEveningTarget();
                hvacMode = todaySched.getEveningHvacMode();
            }
        }
        else if (todaySched.getNightPeriod() == 'P') {
            if (nowPeriod == 'P') {
                if(todaySched.getNightStart() / 100 < now.getHour() - 12) {
                    if(todaySched.getNightStart() % 100 <= now.getMinute()) {
                        targetTemp = todaySched.getNightTarget();
                        hvacMode = todaySched.getNightHvacMode();
                } else {
                        targetTemp = todaySched.getEveningTarget();
                        hvacMode = todaySched.getEveningHvacMode();
                    }
                }
                else {
                    targetTemp = todaySched.getEveningTarget();
                    hvacMode = todaySched.getEveningHvacMode();
                }
            }
        }
        //WERE WE UNABLE TO MATCH THE SCHEDULE TO A CURRENT CONDITION?
        if(targetTemp < 50 || targetTemp > 90) {
            targetTemp = 72;
            
        }
        if (hvacMode != 'A' || hvacMode != 'C' || hvacMode != 'H' || hvacMode != 'O') {
            hvacMode = 'A';
        }
    }
    
    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }
    
    public long getIdentifier() {
        return identifier;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void setThermostatTime(String thermostatTime) {
        this.thermostatTime = thermostatTime;
    }
    
    public String getThermostatTime() {
        return thermostatTime;
    }
    
    public void setUtcTime(String utcTime) {
        this.utcTime = utcTime;
    }
    
    public String getUtcTime() {
        return utcTime;
    }
    
    public void setRuntime(Runtime runtime) {
        this.runtime = runtime;
    }
    
    public Runtime getRuntime() {
        return runtime;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }
    
    public Status getStatus() {
        return status;
    }
    
    public void setTargetTemp(int targetTemp) {
        this.targetTemp = targetTemp;
    }
    
    public int getTargetTemp() {
        return targetTemp;
    }
    
    public void changeTargetTemp(boolean targetUp) {
        if(targetUp && targetTemp < 90) {
            targetTemp++;
        }
        else if (!targetUp && targetTemp > 50) {
            targetTemp--;
        }
    }
    
    public void toggleFan() {
        fanOn = !fanOn;
    }
    
    public boolean isFanOn() {
        return fanOn;
    }
    
    public void setHvacMode(char hvacMode) {
        if(hvacMode == 'A' || hvacMode == 'C' || hvacMode == 'H' || hvacMode == 'O') {
            this.hvacMode = hvacMode;
        }
    }
    
    public void toggleHvacMode() {
        switch (hvacMode) {
            case 'A':
                hvacMode = 'C';
                break;
            case 'C':
                hvacMode = 'H';
                break;
            case 'H':
                hvacMode = 'O';
                break;
            default:
                hvacMode = 'A';
                break;
        }
    }
    
    public char getHvacMode() {
        return hvacMode;
    }
    
    public class Runtime {
        public float actualTemperature;
        public float actualHumidity;
    }
    
    public class Status {
        public int code;
        public String message;
    }
}
