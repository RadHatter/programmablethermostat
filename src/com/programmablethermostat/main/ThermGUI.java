package com.programmablethermostat.main;

import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author aarjs
 * This class handles switching between the program's various screens
 */
public class ThermGUI
        implements MainScreen.MainScreenController,
        ProgScreen.ProgScreenController {
    
    //This interface allows the GUI to communicate back to the parent thermostat controller
    public interface GuiController {
        void saveSchedule(ThermSched thermSched);
        void toggleFan();
        void toggleHvacMode();
        void changeTargetTemp(boolean targetUp);
    }
    
    public GuiController guiController;
    
    //Links the GUI to the parent controller
    public void LinkGuiController(GuiController guiController) {
        this.guiController = guiController;
    }
    
    private Stage thermStage;
    private MainScreen mainScreen;
    private ProgScreen progScreen;
    private final List<Background> backgrounds;    
    private ThermSched savedSched; //The stored schedule is held here to be passed to the programming screen
    
    public ThermGUI() {
        backgrounds = new ArrayList<>();
        backgrounds.add(new Background(new BackgroundFill(Color.rgb(255, 0, 0), CornerRadii.EMPTY, new Insets(-4, -4, -4, -4))));
        backgrounds.add(new Background(new BackgroundFill(Color.rgb(0, 255, 0), CornerRadii.EMPTY, new Insets(-4, -4, -4, -4))));
        backgrounds.add(new Background(new BackgroundFill(Color.rgb(0, 150, 255), CornerRadii.EMPTY, new Insets(-4, -4, -4, -4))));
        backgrounds.add(new Background(new BackgroundFill(Color.rgb(255, 255, 255), CornerRadii.EMPTY, new Insets(-4, -4, -4, -4))));
    }
    
    //Sets the stage for the Thermostat application
    public void setStage(Stage thermStage) {
        this.thermStage = thermStage;
        changeScreen("MAIN");        
        this.thermStage.setResizable(false);
        this.thermStage.show();
    }
    
    /*Attempts to load a saved schedule on first call, otherwise ensures the
     *main screen's background colors are appropriately updated based on the
     *current thermostat status */
    public void updateGUI(ThermStatus thermStatus, ThermSched savedSched) {
        if(savedSched != null)
            this.savedSched = savedSched;
        if(thermStage.getScene() == mainScreen.getMainScene()) {
            mainScreen.updateMainScreen(backgrounds, thermStatus);            
        }
    }
    
    //Swaps betweent the main and programming mode screens
    private void changeScreen(String toScreen) {
        Scene activeScene = null;
        switch(toScreen) {
            case "MAIN":
                if(mainScreen == null) {
                    mainScreen = MainScreen.getMainScreen();
                }
                mainScreen.LinkController(this);
                activeScene = mainScreen.getMainScene();
                break;
            case "PROG":
                if(progScreen == null) {
                    progScreen = ProgScreen.getProgScreen(savedSched);
                }
                progScreen.LinkProgScreenController(this);
                activeScene = progScreen.getProgScene();                
                break;
        }        
        thermStage.setScene(activeScene);
        if(!thermStage.isShowing()) {
            thermStage.show();
        }
    }
    
    @Override
    public void toggleFan() {
        guiController.toggleFan();
    }
    
    @Override
    public void toggleHvacMode() {
        guiController.toggleHvacMode();
    }
    
    public void changeTargetTemp(boolean targetUp) {
        guiController.changeTargetTemp(targetUp);
    }
    
    @Override
    public void enterOperationMode() {
        changeScreen("MAIN");
    }
    
    @Override
    public void enterProgramMode(ThermSched activeSched) {
        changeScreen("PROG");
    }
    
    @Override
    public void saveSchedule(ThermSched thermSched) {
        guiController.saveSchedule(thermSched);
    }
}
