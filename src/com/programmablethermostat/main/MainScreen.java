package com.programmablethermostat.main;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 * @author aarjs
 * This class stores the main handles the main thermostat screen to be
 * displayed during runtime
 */
public class MainScreen {
    
    public interface MainScreenController {
        void enterProgramMode(ThermSched savedSched);
        void toggleFan();
        void toggleHvacMode();
        void changeTargetTemp(boolean targetUp);
    }
    
    private MainScreenController mainController;
    
    public void LinkController(MainScreenController mainController) {
        this.mainController = mainController;
    }
    
    private static MainScreen MAIN_SCREEN = null;    
    private Scene mainScene;
    
    //The gridpane holds all of the visible page elements
    private GridPane mainPane;
    
    //Each label displays a piece of information to the user
    private Label currentTempLabel;
    private Label currentHumidityLabel;
    private Label targetTempLabel;
    private Label fanModeLabel;
    private Label hvacModeLabel;
    private Label clockLabel;
    
    //These buttons toggle the active target temperature
    private Button targetUpBtn;
    private Button targetDnBtn;
    
    private ThermSched activeSched;
    
    private MainScreen() {
        loadMainScreen();
    }    
    
    public static MainScreen getMainScreen() {
        
        if (MAIN_SCREEN == null) {
            MAIN_SCREEN = new MainScreen();
        }
        return MAIN_SCREEN;
    }
    
    public Scene getMainScene() {
        if(mainScene == null) {
            mainScene = new Scene(mainPane, 800, 600);
        }
        return mainScene;
    }
    
    public void updateMainScreen(List<Background> backgrounds, ThermStatus currentStatus) {
        currentTempLabel.setText(String.valueOf(currentStatus.getRuntime().actualTemperature));
        if(currentStatus.getRuntime().actualTemperature - currentStatus.getTargetTemp() > 1) {
            currentTempLabel.setBackground(backgrounds.get(0));
        }
        else if(currentStatus.getRuntime().actualTemperature - currentStatus.getTargetTemp() < -1) {
            currentTempLabel.setBackground(backgrounds.get(2));
        }
        else {
            currentTempLabel.setBackground(backgrounds.get(1));
        }
        currentHumidityLabel.setText(String.valueOf(currentStatus.getRuntime().actualHumidity) + "%");
        if(currentStatus.getRuntime().actualHumidity < 30) {
            currentHumidityLabel.setBackground(backgrounds.get(2));
        }
        else if(currentStatus.getRuntime().actualHumidity > 60) {
            currentHumidityLabel.setBackground(backgrounds.get(0));
        }
        else {
            currentHumidityLabel.setBackground(backgrounds.get(1));
        }
        
        targetTempLabel.setText(String.valueOf(currentStatus.getTargetTemp()));
        if(currentStatus.isFanOn()) {
            fanModeLabel.setText("ON");
            fanModeLabel.setBackground(backgrounds.get(1));
        }
        else {
            fanModeLabel.setText("AUTO");
            fanModeLabel.setBackground(backgrounds.get(3));
        }
        
        switch(currentStatus.getHvacMode()) {
            case 'A':
                hvacModeLabel.setText("AUTO");
                hvacModeLabel.setBackground(backgrounds.get(1));
                targetTempLabel.setBackground(backgrounds.get(1));
                break;
            case 'C':
                hvacModeLabel.setText("COOL");
                hvacModeLabel.setBackground(backgrounds.get(2));
                targetTempLabel.setBackground(backgrounds.get(2));
                break;
            case 'H':
                hvacModeLabel.setText("HEAT");
                hvacModeLabel.setBackground(backgrounds.get(0));
                targetTempLabel.setBackground(backgrounds.get(0));
                break;
            case 'O':
                hvacModeLabel.setText("OFF");
                hvacModeLabel.setBackground(backgrounds.get(3));
                targetTempLabel.setBackground(backgrounds.get(3));
                break;
            default:
                hvacModeLabel.setText("ERROR");
                hvacModeLabel.setBackground(backgrounds.get(0));
                targetTempLabel.setBackground(backgrounds.get(2));
                break;
        
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm aa");
        Date now = new Date(System.currentTimeMillis());
        clockLabel.setText(dateFormat.format(now));
        clockLabel.setBackground(backgrounds.get(3));
    }
    
    //Loads the main screen pane for thermostat operation
    private void loadMainScreen() {
        mainPane = new GridPane();
        mainPane.setAlignment(Pos.CENTER);
        mainPane.hgapProperty().setValue(24);
        mainPane.vgapProperty().setValue(32);
        mainPane.paddingProperty().setValue(new Insets(32, 32, 32, 32));
        
        mainPane.add(loadCurrentTemp(), 0, 0);
        mainPane.add(loadTempUnits(), 1, 0);
        mainPane.add(loadHumidity(), 0, 1);
        mainPane.add(loadTargetTemp(), 0, 2);
        mainPane.add(loadTargetTempBtns(), 1, 2);
        mainPane.add(loadClock(), 2, 2, 2, 1);
        mainPane.add(loadSystemControls(), 3, 0, 1, 2);
        mainPane.add(loadProgramBtn(), 0, 3, 4, 1);
    }
    
    //Displays the current temperature as retrieved from the thermostat via the web
    private VBox loadCurrentTemp() {
        VBox temperatureBox = new VBox();
        temperatureBox.setSpacing(8);
        temperatureBox.setAlignment(Pos.CENTER);
        Label temperatureBoxLabel = new Label("Current Temperature");
        temperatureBoxLabel.setFont(new Font("Arial", 18));
        
        HBox temperatureDataBox = new HBox();
        temperatureDataBox.setSpacing(16);
        temperatureDataBox.setAlignment(Pos.CENTER);
        currentTempLabel = new Label("0.0");
        currentTempLabel.setFont(new Font("Arial", 28));   
        
        temperatureDataBox.getChildren().add(currentTempLabel);
        temperatureBox.getChildren().addAll(temperatureBoxLabel, temperatureDataBox);
        return temperatureBox;
    }
    
    //Loads radio buttons to allow switching from metric to standard
    private VBox loadTempUnits() {
        VBox unitRadioBox = new VBox();
        unitRadioBox.setAlignment(Pos.BOTTOM_LEFT);
        
        ToggleGroup unitToggle = new ToggleGroup();
        unitToggle.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle selection, Toggle previous) -> {
            DecimalFormat tempFormat = new DecimalFormat("###.#");
            if(unitToggle.getSelectedToggle().getUserData() == "F") {
                float celsiusTemp = Float.parseFloat(currentTempLabel.getText());
                double farenheitTemp = (celsiusTemp * 1.8) + 32.0;
                currentTempLabel.setText(tempFormat.format(farenheitTemp));
            }
            else if (unitToggle.getSelectedToggle().getUserData() == "C") {
                float farenheitTemp = Float.parseFloat(currentTempLabel.getText());
                double celsiusTemp = (farenheitTemp - 32) * 0.5556;
                currentTempLabel.setText(tempFormat.format(celsiusTemp));
            }
        });
        
        RadioButton farenheitBtn = new RadioButton("Farenheit");
        farenheitBtn.setUserData("F");
        RadioButton celsiusBtn = new RadioButton("Celsius");
        celsiusBtn.setUserData("C");
        farenheitBtn.setToggleGroup(unitToggle);
        farenheitBtn.setSelected(true);
        celsiusBtn.setToggleGroup(unitToggle);        
        unitRadioBox.getChildren().addAll(farenheitBtn, celsiusBtn);
        return unitRadioBox;
    }
    
    //Loads the humidity level display
    private VBox loadHumidity() {
        VBox currentHumidityBox = new VBox();
        currentHumidityBox.setSpacing(8);
        currentHumidityBox.setAlignment(Pos.CENTER);
        
        Label humidityBoxLabel = new Label("Current Humidity");
        humidityBoxLabel.setFont(new Font("Arial", 18));
        
        currentHumidityLabel = new Label("0%");
        currentHumidityLabel.setFont(new Font("Arial", 28));
        
        currentHumidityBox.getChildren().addAll(humidityBoxLabel, currentHumidityLabel);
        return currentHumidityBox;
    }
    
    //Loads the target temperature display
    private HBox loadTargetTemp() {
        HBox targetTempBox = new HBox();
        targetTempBox.setAlignment(Pos.CENTER);
        targetTempBox.setSpacing(8);
        
        VBox currentTargetTempBox = new VBox();
        currentTargetTempBox.setAlignment(Pos.CENTER);
        currentTargetTempBox.setSpacing(8);
        Label targetTempBoxLabel = new Label("Target Temperature");
        targetTempBoxLabel.setFont(new Font("Arial", 18));
        targetTempLabel = new Label("0");
        targetTempLabel.setFont(new Font("Arial", 28));

        currentTargetTempBox.getChildren().addAll(targetTempBoxLabel, targetTempLabel);
        
        targetTempBox.getChildren().add(currentTargetTempBox);
        return targetTempBox;
    }
    
    //Loads buttons to adjust the target temperature
    private VBox loadTargetTempBtns() {
        VBox currentTargetBtnBox = new VBox();
        currentTargetBtnBox.setAlignment(Pos.BOTTOM_LEFT);
        currentTargetBtnBox.setSpacing(8);
        targetUpBtn = new Button("Up");
        targetUpBtn.setOnAction((ActionEvent tempUp) -> {
            mainController.changeTargetTemp(true);
        });
        targetDnBtn = new Button("Dn");
        targetDnBtn.setOnAction((ActionEvent tempDn) -> {
            mainController.changeTargetTemp(false);
        });
        currentTargetBtnBox.getChildren().addAll(targetUpBtn, targetDnBtn);
        return currentTargetBtnBox;
    }
    
    //Loads system controls like fan mode and hvac mode
    private VBox loadSystemControls() {
        VBox systemControlBox = new VBox();
        systemControlBox.setAlignment(Pos.CENTER);
        systemControlBox.setSpacing(16);        
        
        VBox fanControls = new VBox();
        fanControls.setAlignment(Pos.CENTER);
        fanControls.setSpacing(8);
        Button fanBtn = new Button("Fan Mode");
        fanBtn.setOnAction((ActionEvent toggleFan) -> {
            mainController.toggleFan();
        });
        fanModeLabel = new Label("AUTO");
        fanModeLabel.setFont(new Font("Arial", 16));

        fanControls.getChildren().addAll(fanBtn, fanModeLabel);
        
        VBox hvacControls = new VBox();
        hvacControls.setAlignment(Pos.CENTER);
        hvacControls.setSpacing(8);
        Button hvacBtn = new Button("HVAC Mode");
        hvacBtn.setOnAction((ActionEvent toggleHvac) -> {
            mainController.toggleHvacMode();
        });
        hvacModeLabel = new Label("AUTO");
        hvacModeLabel.setFont(new Font("Arial", 16));

        hvacControls.getChildren().addAll(hvacBtn, hvacModeLabel);
        
        systemControlBox.getChildren().addAll(fanControls, hvacControls);
        return systemControlBox;
    }    
        
    //Loads the clock
    private VBox loadClock() {
        VBox clockBox = new VBox();
        clockBox.setAlignment(Pos.CENTER);
        clockBox.setSpacing(8);
        
        Label clockBoxLabel = new Label("Current Time");
        clockBoxLabel.setFont(new Font("Arial", 18));
        
        clockLabel = new Label("HH:mm DDDD");
        clockLabel.setFont(new Font("Arial", 24));
        
        clockBox.getChildren().addAll(clockBoxLabel, clockLabel);
        return clockBox;
    }
    
    //Loads the program button for future implemention
    private HBox loadProgramBtn() {
        HBox programBtnBox = new HBox();
        programBtnBox.setAlignment(Pos.CENTER);
        
        Button programBtn = new Button("Program");
        programBtn.setFont(new Font("Arial", 24));
        programBtn.setOnAction((ActionEvent progMode) -> {
            mainController.enterProgramMode(activeSched);
        });
        
        programBtnBox.getChildren().add(programBtn);
        return programBtnBox;
    }
}
