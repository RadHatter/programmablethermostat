package com.programmablethermostat.main;

import java.time.LocalDateTime;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 * @author aarjs
 */
public class ProgScreen {
    
    public interface ProgScreenController {
        void saveSchedule(ThermSched saveSchedule);
        void enterOperationMode();
    }
    
    public void LinkProgScreenController(ProgScreenController progController) {
        this.progScreenController = progController;
    }
    
    private ProgScreenController progScreenController;
    
    private HBox timeOfDay;
    private HBox timePicker;
    private HBox targetSettings;
    private HBox modeSettings;    
    private HBox holidayPicker;
    private HBox holidayBtns;
    private Button addHolidayBtn;
    private Button delHolidayBtn;
    private Button resetBtn;
    
    private static ProgScreen PROG_SCREEN = null;    
    private Scene progScene;    
    private GridPane progPane;
    
    private ThermSched editSched;
    
    private Label thisDayLabel;
    private Label thisTimeOfDayLabel;
    private Label thisTimeLabel;
    private Label targetTempLabel;
    private Label hvacModeLabel;
    private Label holidayMonthLabel;
    private Label holidayDayLabel;    
    private Label holidayYearLabel;
    private int selectedDayIndex;
    private int selectedTodIndex;
    private int holidayDay;
    private int holidayMonth;
    private int holidayYear;
    
    private ProgScreen(ThermSched savedSched) {
        editSched = savedSched;
        loadProgScreen();
    }
    
    public static ProgScreen getProgScreen(ThermSched savedSched) {
        if (PROG_SCREEN == null) {
            PROG_SCREEN = new ProgScreen(savedSched);
        }
        return PROG_SCREEN;
    }
    
    public Scene getProgScene() {        
        if(progScene == null) {
            progScene = new Scene(progPane, 800, 600);
            if(editSched == null) {
                editSched = new ThermSched();
                updateProgScreen();
            }
        }
        return progScene;
    }

    public void updateProgScreen() {
        switch(selectedDayIndex) {
            case 0:
                hideHolidayPicker();
                fillProgLabels(editSched.getDailySched(0));
                break;
            case 1:
                fillProgLabels(editSched.getDailySched(1));
                break;
            case 2:
                fillProgLabels(editSched.getDailySched(2));
                break;
            case 3:
                fillProgLabels(editSched.getDailySched(3));
                break;
            case 4:
                fillProgLabels(editSched.getDailySched(4));
                break;
            case 5:
                fillProgLabels(editSched.getDailySched(5));
                break;
            case 6:
                fillProgLabels(editSched.getDailySched(6));
                break;
            case 7:
                fillProgLabels(editSched.getDailySched(7));
                break;
            case 8:
                fillProgLabels(editSched.getDailySched(8));
                break;
            case 9:
                hideHolidayPicker();
                fillProgLabels(editSched.getDailySched(9));
                break;
            case 10:
                showHolidayPicker(true);
                fillProgLabels(editSched.getHolidaySched(0));
                holidayMonthLabel.setText(String.valueOf(holidayMonth));
                holidayDayLabel.setText(String.valueOf(holidayDay));
                holidayYearLabel.setText(String.valueOf(holidayYear));
                break;
            default:
                showHolidayPicker(false);                
                fillProgLabels(editSched.getHolidaySched(selectedDayIndex - 10));
                holidayMonth = editSched.getHolidaySched(selectedDayIndex - 10).getHoliday().holiMonth;
                holidayDay = editSched.getHolidaySched(selectedDayIndex - 10).getHoliday().holiDay;
                holidayYear = editSched.getHolidaySched(selectedDayIndex - 10).getHoliday().holiYear;
                holidayMonthLabel.setText(String.valueOf(holidayMonth));
                holidayDayLabel.setText(String.valueOf(holidayDay));
                holidayYearLabel.setText(String.valueOf(holidayYear));
                break;
        }
    }
    
    private void fillProgLabels(ThermSched.DailySched dailySched) {
        thisDayLabel.setText(dailySched.getDay());
        switch(selectedTodIndex) {
            case 0:
                thisTimeOfDayLabel.setText("Morning");
                thisTimeLabel.setText(dailySched.getMorningStartString());
                targetTempLabel.setText(String.valueOf(dailySched.getMorningTarget()));
                hvacModeLabel.setText(getHvacModeLabel(dailySched.getMorningHvacMode()));
                break;
            case 1:
                thisTimeOfDayLabel.setText("Evening");
                thisTimeLabel.setText(dailySched.getEveningStartString());
                targetTempLabel.setText(String.valueOf(dailySched.getEveningTarget()));
                hvacModeLabel.setText(getHvacModeLabel(dailySched.getEveningHvacMode()));
                break;
            case 2:
                thisTimeOfDayLabel.setText("Night");
                thisTimeLabel.setText(dailySched.getNightStartString());
                targetTempLabel.setText(String.valueOf(dailySched.getNightTarget()));
                hvacModeLabel.setText(getHvacModeLabel(dailySched.getNightHvacMode()));
                break;
            default:
                thisTimeOfDayLabel.setText("ERROR");
                targetTempLabel.setText(String.valueOf(-1));
                break;
        }
    }
    
    private String getHvacModeLabel(char hvacMode) {
        switch (hvacMode) {
            case 'A':
                return "Auto";
            case 'C':
                return "Cool";
            case 'H':
                return "Heat";
            case 'O':
                return "Off";
            default:
                return "ERROR";
        }                
    }
    
    private void loadProgScreen() {
        LocalDateTime now = LocalDateTime.now();
        holidayDay = now.getDayOfMonth();
        holidayMonth = now.getMonthValue();
        holidayYear = now.getYear();
        
        if(editSched == null)
            editSched = new ThermSched();
        selectedDayIndex = 0;
        selectedTodIndex = 0;
        progPane = new GridPane();
        progPane.setAlignment(Pos.CENTER);
        progPane.setHgap(32);
        progPane.setVgap(48);
        progPane.paddingProperty().setValue(new Insets(32, 32, 32, 32));
        
        progPane.add(loadDayPicker(), 0, 0, 1, 1);
        progPane.add(loadHolidayPicker(), 1, 0, 1, 1);
        holidayPicker.setVisible(false);
        
        progPane.add(loadTimeOfDay(), 0, 1);
        progPane.add(loadTimePicker(), 1, 1);
        progPane.add(loadTargetSettings(), 0, 2);
        progPane.add(loadModeSettings(), 1, 2);
        progPane.add(loadConfirmBtns(), 0, 3, 1, 1);
        progPane.add(loadHolidayBtns(), 1, 3, 2, 1);
        updateProgScreen();
    }
    
    private HBox loadDayPicker() {
        HBox dayPickerBox = new HBox();
        dayPickerBox.setSpacing(16);
        
        VBox dayBox = new VBox();
        dayBox.setSpacing(8);
        dayBox.setAlignment(Pos.CENTER_LEFT);
        Label dayBoxLabel = new Label("EDIT DAY");
        dayBoxLabel.setFont(new Font("Arial", 20));
        thisDayLabel = new Label("Thisday");
        thisDayLabel.setFont(new Font("Arial", 28));
        dayBox.getChildren().addAll(dayBoxLabel, thisDayLabel);    
        
        HBox dayBtns = new HBox();
        dayBtns.setSpacing(2);
        dayBtns.setPadding(new Insets(0, 0, 8, 0));
        dayBtns.setAlignment(Pos.BOTTOM_RIGHT);
        Button prevDayBtn = new Button("PREV");
        
        prevDayBtn.setOnAction((ActionEvent prevDay) -> {
            if(selectedDayIndex == 0) {
                selectedDayIndex = 10 + editSched.getHolidayCount();
            } else {
                selectedDayIndex--;
            }
            updateProgScreen();
        });
        Button nextDayBtn = new Button("NEXT"); 
        nextDayBtn.setOnAction((ActionEvent nextDay) -> {
            if(selectedDayIndex == 10 + editSched.getHolidayCount()) {
                selectedDayIndex = 0;
            } else {
                selectedDayIndex++;
            }
            updateProgScreen();
        });
        dayBtns.getChildren().addAll(prevDayBtn, nextDayBtn);
        
        dayPickerBox.getChildren().addAll(dayBox, dayBtns);
        return dayPickerBox;        
    }
    
    private HBox loadTimeOfDay() {
        timeOfDay = new HBox();
        timeOfDay.setAlignment(Pos.CENTER);
        
        VBox timeOfDayBox = new VBox();
        timeOfDayBox.setSpacing(8);
        Label timeOfDayBoxLabel = new Label("TIME OF DAY");
        timeOfDayBoxLabel.setFont(new Font("Arial", 20));
        thisTimeOfDayLabel = new Label("Daytime");
        thisTimeOfDayLabel.setFont(new Font("Arial", 28));
        timeOfDayBox.getChildren().addAll(timeOfDayBoxLabel, thisTimeOfDayLabel);
        
        HBox timeOfDayBtns = new HBox();
        timeOfDayBtns.setSpacing(2);
        timeOfDayBtns.setPadding(new Insets(0, 0, 8, 0));
        timeOfDayBtns.setAlignment(Pos.BOTTOM_LEFT);
        Button prevTodBtn = new Button("PREV");
        prevTodBtn.setOnAction((ActionEvent prevTod) -> {
            if(selectedTodIndex == 0) {
                selectedTodIndex = 2;
            }
            else {
                selectedTodIndex--;
            }
            updateProgScreen();
        });
        Button nextTodBtn = new Button("NEXT");
        nextTodBtn.setOnAction((ActionEvent prevTod) -> {
            if(selectedTodIndex == 2) {
                selectedTodIndex = 0;
            }
            else {
                selectedTodIndex++;
            }
            updateProgScreen();
        });
        timeOfDayBtns.getChildren().addAll(prevTodBtn, nextTodBtn);
        
        timeOfDay.getChildren().addAll(timeOfDayBox, timeOfDayBtns);
        return timeOfDay;
    }
    
    private HBox loadTimePicker() {
        timePicker = new HBox();
        timePicker.setAlignment(Pos.CENTER);
        timePicker.setSpacing(32);
        
        VBox timeBox = new VBox();
        timeBox.setSpacing(8);
        Label timeBoxLabel = new Label("START AT");
        timeBoxLabel.setFont(new Font("Arial", 20));
        thisTimeLabel = new Label("0:00XM");
        thisTimeLabel.setFont(new Font("Arial", 28));
        timeBox.getChildren().addAll(timeBoxLabel, thisTimeLabel);
        
        HBox timeBtns = new HBox();
        timeBtns.setSpacing(2);
        timeBtns.setPadding(new Insets(0, 0, 8, 0));
        timeBtns.setAlignment(Pos.BOTTOM_LEFT);
        Button hourBtn = new Button("HOUR");
        hourBtn.setOnAction((ActionEvent incrementHour) -> {
            ThermSched.DailySched activeSched = null;
            if(selectedDayIndex < 10)
                activeSched = editSched.getDailySched(selectedDayIndex);
            else
                activeSched = editSched.getHolidaySched(selectedDayIndex - 10);
            switch(selectedTodIndex) {
                case 0:
                    activeSched.increaseMorningHour();
                    break;
                case 1:
                    activeSched.increaseEveningHour();
                    break;
                case 2:
                    activeSched.increaseNightHour();
                    break;
            }
            switch (selectedDayIndex) {                
                case 1:                      
                case 2:
                case 3:
                case 4:
                case 5:
                    editSched.desyncWeekDays();
                    editSched.desyncEveryDay();
                    break;
                case 0:
                case 6:
                    editSched.desyncWeekendDays();
                    editSched.desyncEveryDay();
                    break;
                case 7:
                    editSched.syncWeekDays();
                    editSched.desyncEveryDay();
                    break;
                case 8:
                    editSched.syncWeekendDays();
                    editSched.desyncEveryDay();
                    break;
                case 9:
                    editSched.syncEveryDay();
                default:
                    break;
            }
            updateProgScreen();
        });
        Label timeDivider = new Label(":");
        timeDivider.setFont(new Font("Arial", 20));
        Button minBtn = new Button("MIN");
        minBtn.setOnAction((ActionEvent incrementMin) -> {
            ThermSched.DailySched activeSched = null;
            if(selectedDayIndex < 10)
                activeSched = editSched.getDailySched(selectedDayIndex);
            else
                activeSched = editSched.getHolidaySched(selectedDayIndex - 10);
            switch(selectedTodIndex) {
                case 0:
                    activeSched.increaseMorningMinute();
                    break;
                case 1:
                    activeSched.increaseEveningMinute();
                    break;
                case 2:
                    activeSched.increaseNightMinute();
                    break;
            }
            switch (selectedDayIndex) {
                case 1:                      
                case 2:
                case 3:
                case 4:
                case 5:
                    editSched.desyncWeekDays();
                    editSched.desyncEveryDay();
                    break;
                case 0:
                case 6:
                    editSched.desyncWeekendDays();
                    editSched.desyncEveryDay();
                    break;
                case 7:
                    editSched.syncWeekDays();
                    editSched.desyncEveryDay();
                    break;
                case 8:
                    editSched.syncWeekendDays();
                    editSched.desyncEveryDay();
                    break;
                case 9:
                    editSched.syncEveryDay();
                default:
                    break;
            }
            updateProgScreen();
        });
        Button amPmBtn = new Button("AM/PM");
        amPmBtn.setOnAction((ActionEvent toggleAmPm) -> {
            ThermSched.DailySched activeSched = null;
            if(selectedDayIndex < 10)
                activeSched = editSched.getDailySched(selectedDayIndex);
            else
                activeSched = editSched.getHolidaySched(selectedDayIndex - 10);
            switch(selectedTodIndex) {
                case 0:
                    activeSched.toggleMorningPeriod();
                    break;
                case 1:
                    activeSched.toggleEveningPeriod();
                    break;
                case 2:
                    activeSched.toggleNightPeriod();
                    break;
            }
            switch (selectedDayIndex) {
                case 1:                      
                case 2:
                case 3:
                case 4:
                case 5:
                    editSched.desyncWeekDays();
                    editSched.desyncEveryDay();
                    break;
                case 0:
                case 6:
                    editSched.desyncWeekendDays();
                    editSched.desyncEveryDay();
                    break;
                case 7:
                    editSched.syncWeekDays();
                    editSched.desyncEveryDay();
                    break;
                case 8:
                    editSched.syncWeekendDays();
                    editSched.desyncEveryDay();
                    break;
                case 9:
                    editSched.syncEveryDay();
                default:
                    break;
            }
            updateProgScreen();
        });
        timeBtns.getChildren().addAll(hourBtn, timeDivider, minBtn, amPmBtn);
        
        timePicker.getChildren().addAll(timeBox, timeBtns);
        return timePicker;
    }
    
    private HBox loadTargetSettings() {
        targetSettings = new HBox();
        targetSettings.setAlignment(Pos.CENTER);
        targetSettings.setSpacing(32);
        
        VBox targetTempBox = new VBox();
        targetTempBox.setSpacing(8);
        Label targetTempBoxLabel = new Label("TARGET TEMP");
        targetTempBoxLabel.setFont(new Font("Arial", 20));
        targetTempLabel = new Label("00");
        targetTempLabel.setFont(new Font("Arial", 28));
        targetTempBox.getChildren().addAll(targetTempBoxLabel, targetTempLabel);
        
        VBox targetTempBtns = new VBox();
        targetTempBtns.setSpacing(2);
        targetTempBtns.setPadding(new Insets(0, 0, 8, 0));
        targetTempBtns.setAlignment(Pos.CENTER_LEFT);
        Button tempUpBtn = new Button("UP");
        tempUpBtn.setOnAction((ActionEvent targetUp) -> {
            ThermSched.DailySched activeSched = null;
            if(selectedDayIndex < 10)
                activeSched = editSched.getDailySched(selectedDayIndex);
            else
                activeSched = editSched.getHolidaySched(selectedDayIndex - 10);
            switch(selectedTodIndex) {
                case 0:
                    activeSched.increaseMorningTarget();
                    break;
                case 1:
                    activeSched.increaseEveningTarget();
                    break;
                case 2:
                    activeSched.increaseNightTarget();
                    break;
            }
            switch (selectedDayIndex) {
                case 1:                      
                case 2:
                case 3:
                case 4:
                case 5:
                    editSched.desyncWeekDays();
                    editSched.desyncEveryDay();
                    break;
                case 0:
                case 6:
                    editSched.desyncWeekendDays();
                    editSched.desyncEveryDay();
                    break;
                case 7:
                    editSched.syncWeekDays();
                    editSched.desyncEveryDay();
                    break;
                case 8:
                    editSched.syncWeekendDays();
                    editSched.desyncEveryDay();
                    break;
                case 9:
                    editSched.syncEveryDay();
                default:
                    break;
            }
            updateProgScreen();
        });
        Button tempDnBtn = new Button("DN"); 
        tempDnBtn.setOnAction((ActionEvent targetDn) -> {
            ThermSched.DailySched activeSched = null;
            if(selectedDayIndex < 10)
                activeSched = editSched.getDailySched(selectedDayIndex);
            else
                activeSched = editSched.getHolidaySched(selectedDayIndex - 10);
            switch(selectedTodIndex) {
                case 0:
                    activeSched.decreaseMorningTarget();
                case 1:
                    activeSched.decreaseEveningTarget();
                    break;
                case 2:
                    activeSched.decreaseNightTarget();
                    break;
            }
            if(selectedDayIndex == 7)
                editSched.syncWeekDays();
            else if(selectedDayIndex == 0 || selectedDayIndex == 6) {
                editSched.desyncWeekendDays();
            }
            updateProgScreen();
        });
        targetTempBtns.getChildren().addAll(tempUpBtn, tempDnBtn);
        
        targetSettings.getChildren().addAll(targetTempBox, targetTempBtns);
        return targetSettings;
    }
    
    private HBox loadModeSettings() {
        modeSettings = new HBox();
        
        VBox hvacModeBox = new VBox();
        hvacModeBox.setSpacing(8);
        Label hvacModeBoxLabel = new Label("HVAC MODE");
        hvacModeBoxLabel.setFont(new Font("Arial", 20));
        hvacModeLabel = new Label("Auto");
        hvacModeLabel.setFont(new Font("Arial", 28));
        hvacModeBox.getChildren().addAll(hvacModeBoxLabel, hvacModeLabel);
        
        HBox hvacModeBtns = new HBox();
        hvacModeBtns.setSpacing(2);
        hvacModeBtns.setPadding(new Insets(0, 0, 8, 0));
        hvacModeBtns.setAlignment(Pos.BOTTOM_LEFT);
        Button prevModeBtn = new Button("<");
        prevModeBtn.setOnAction((ActionEvent prevMOde) -> {
            ThermSched.DailySched activeSched = null;
            if(selectedDayIndex < 10)
                activeSched = editSched.getDailySched(selectedDayIndex);
            else
                activeSched = editSched.getHolidaySched(selectedDayIndex - 10);
            switch(selectedTodIndex) {
                case 0:
                    activeSched.prevMorningHvacMode();
                case 1:
                    activeSched.prevEveningHvacMode();
                    break;
                case 2:
                    activeSched.prevNightHvacMode();
                    break;
            }
            switch (selectedDayIndex) {
                case 1:                      
                case 2:
                case 3:
                case 4:
                case 5:
                    editSched.desyncWeekDays();
                    editSched.desyncEveryDay();
                    break;
                case 0:
                case 6:
                    editSched.desyncWeekendDays();
                    editSched.desyncEveryDay();
                    break;
                case 7:
                    editSched.syncWeekDays();
                    editSched.desyncEveryDay();
                    break;
                case 8:
                    editSched.syncWeekendDays();
                    editSched.desyncEveryDay();
                    break;
                case 9:
                    editSched.syncEveryDay();
                default:
                    break;
            }
            updateProgScreen();
        });
        Button nextModeBtn = new Button(">");
        nextModeBtn.setOnAction((ActionEvent nextMode) -> {
            ThermSched.DailySched activeSched = null;
            if(selectedDayIndex < 10)
                activeSched = editSched.getDailySched(selectedDayIndex);
            else
                activeSched = editSched.getHolidaySched(selectedDayIndex - 10);
            switch(selectedTodIndex) {
                case 0:
                    activeSched.nextMorningHvacMode();
                    break;
                case 1:
                    activeSched.nextEveningHvacMode();
                    break;
                case 2:
                    activeSched.nextNightHvacMode();
                    break;
            }
            switch (selectedDayIndex) {
                case 1:                      
                case 2:
                case 3:
                case 4:
                case 5:
                    editSched.desyncWeekDays();
                    editSched.desyncEveryDay();
                    break;
                case 0:
                case 6:
                    editSched.desyncWeekendDays();
                    editSched.desyncEveryDay();
                    break;
                case 7:
                    editSched.syncWeekDays();
                    editSched.desyncEveryDay();
                    break;
                case 8:
                    editSched.syncWeekendDays();
                    editSched.desyncEveryDay();
                    break;
                case 9:
                    editSched.syncEveryDay();
                default:
                    break;
            }
            updateProgScreen();
        });
        hvacModeBtns.getChildren().addAll(prevModeBtn, nextModeBtn);        
        modeSettings.getChildren().addAll(hvacModeBox, hvacModeBtns);
        return modeSettings;
    }
    
    private HBox loadHolidayPicker() {
        holidayPicker = new HBox();
        holidayPicker.setAlignment(Pos.CENTER);
        holidayPicker.setSpacing(16);
        
        VBox monthBox = new VBox();
        monthBox.setAlignment(Pos.CENTER);
        monthBox.setSpacing(8);
        holidayMonthLabel = new Label(String.valueOf(holidayDay));
        holidayMonthLabel.setFont(new Font("Arial", 24));
        HBox monthBtnBox = new HBox();
        monthBtnBox.setAlignment(Pos.CENTER);
        Button monthDn = new Button("<");
        monthDn.setOnAction((ActionEvent holidayMonthDn) -> {
            holidayMonth--;
            verifyHolidayDate();
        });
        Button monthUp = new Button(">");
        monthUp.setOnAction((ActionEvent holidayMonthUp) -> {
            holidayMonth++;
            verifyHolidayDate();
        });
        monthBtnBox.getChildren().addAll(monthDn, monthUp);
        monthBox.getChildren().addAll(holidayMonthLabel, monthBtnBox);
        
        VBox dayBox = new VBox();
        dayBox.setAlignment(Pos.CENTER);
        dayBox.setSpacing(8);
        holidayDayLabel = new Label(String.valueOf(holidayMonth));
        holidayDayLabel.setFont(new Font("Arial", 24));
        HBox dayBtnBox = new HBox();
        dayBtnBox.setAlignment(Pos.CENTER);
        Button dayDn = new Button("<");
        dayDn.setOnAction((ActionEvent holidayDayDn) -> {
            holidayDay--;
            verifyHolidayDate();
        });
        Button dayUp = new Button(">");
        dayUp.setOnAction((ActionEvent holidayDayUp) -> {
            holidayDay++;
            verifyHolidayDate();
        });
        dayBtnBox.getChildren().addAll(dayDn, dayUp);
        dayBox.getChildren().addAll(holidayDayLabel, dayBtnBox);

        VBox yearBox = new VBox();
        yearBox.setAlignment(Pos.CENTER);
        yearBox.setSpacing(8);
        holidayYearLabel = new Label(String.valueOf(holidayYear));
        holidayYearLabel.setFont(new Font("Arial", 24));
        HBox yearBtnBox = new HBox();
        yearBtnBox.setAlignment(Pos.CENTER);
        Button yearDn = new Button("<");
        yearDn.setOnAction((ActionEvent holidayYearDn) -> {
            holidayYear--;
            verifyHolidayDate();
        });
        Button yearUp = new Button(">");
        yearUp.setOnAction((ActionEvent holidayYearUp) -> {
            holidayYear++;
            verifyHolidayDate();
        });
        yearBtnBox.getChildren().addAll(yearDn, yearUp);
        yearBox.getChildren().addAll(holidayYearLabel, yearBtnBox);        
        
        holidayPicker.getChildren().addAll(monthBox, dayBox, yearBox);
        
        return holidayPicker;
    }
    
    private void verifyHolidayDate() {
        verifyMonth();
        verifyDay();
        editSched.getHolidaySched(selectedDayIndex - 10).getHoliday().holiDay = holidayDay;
        editSched.getHolidaySched(selectedDayIndex - 10).getHoliday().holiMonth = holidayMonth;
        editSched.getHolidaySched(selectedDayIndex - 10).getHoliday().holiYear = holidayYear;
        updateProgScreen();
    }
    
    private void verifyMonth() {
        if(holidayMonth < 1)
            holidayMonth = 12;
        if(holidayMonth > 12)
            holidayMonth = 1;
    }
    
    private void verifyDay() {
        int daysInFeb = 28;
        if(isLeapYear())
            daysInFeb++;
        switch(holidayMonth) {
            case 1:
                if(holidayDay > 31) {
                    holidayDay = 1;
                    holidayMonth++;
                }
                else if (holidayDay < 1) {
                    holidayDay = 31;
                    holidayMonth = 12;
                    holidayYear--;
                }
                break;
            case 2:
                if(holidayDay > daysInFeb) {
                    holidayDay = 1;
                    holidayMonth++;
                } else if(holidayDay < 1) {
                    holidayDay = 31;
                    holidayMonth--;
                }
                break;
            case 3:
                if(holidayDay > 31) {
                    holidayDay = 1;
                    holidayMonth++;
                } else if(holidayDay < 1) {
                    holidayDay = daysInFeb;
                    holidayMonth--;
                }
                break;
            case 4:
                if(holidayDay > 30) {
                    holidayDay = 1;
                    holidayMonth++;
                } else if(holidayDay < 1) {
                    holidayDay = 31;
                    holidayMonth--;
                }
                break;
            case 5:
                if(holidayDay > 31) {
                    holidayDay = 1;
                    holidayMonth++;
                } else if(holidayDay < 1) {
                    holidayDay = 30;
                    holidayMonth--;
                }
                break;
            case 6:
                if(holidayDay > 30) {
                    holidayDay = 1;
                    holidayMonth++;
                } else if(holidayDay < 1) {
                    holidayDay = 31;
                    holidayMonth--;
                }
                break;
            case 7:
                if(holidayDay > 31) {
                    holidayDay = 1;
                    holidayMonth++;
                } else if(holidayDay < 1) {
                    holidayDay = 30;
                    holidayMonth--;
                }
                break;
            case 8:
                if(holidayDay > 31) {
                    holidayDay = 1;
                    holidayMonth++;
                } else if(holidayDay < 1) {
                    holidayDay = 31;
                    holidayMonth--;
                }
                break;
            case 9:
                if(holidayDay > 30) {
                    holidayDay = 1;
                    holidayMonth++;
                } else if(holidayDay < 1) {
                    holidayDay = 31;
                    holidayMonth--;
                }
                break;
            case 10:
                if(holidayDay > 31) {
                    holidayDay = 1;
                    holidayMonth++;
                } else if(holidayDay < 1) {
                    holidayDay = 30;
                    holidayMonth--;
                }
                break;
            case 11:
                if(holidayDay > 30) {
                    holidayDay = 1;
                    holidayMonth++;
                } else if(holidayDay < 1) {
                    holidayDay = 31;
                    holidayMonth--;
                }
                break;
            case 12:
                if(holidayDay > 31) {
                    holidayDay = 1;
                    holidayMonth = 1;
                    holidayYear++;
                } else if(holidayDay < 1) {
                    holidayDay = 30;
                    holidayMonth--;
                }
                break;
        }
    }
    
    private boolean isLeapYear() {
        if(holidayYear % 4 == 0) {
            if(holidayYear % 100 == 0) {
                if(holidayYear % 400 == 0)
                    return true;
                else
                    return false;
            }
            else
                return true;
        }
        else
            return false;
    }
    
    private HBox loadConfirmBtns() {
        HBox confirmBtnBox = new HBox();
        confirmBtnBox.setAlignment(Pos.CENTER);
        confirmBtnBox.setSpacing(8);
        
        Button saveBtn = new Button("SAVE");
        saveBtn.setFont(new Font("Arial", 18));
        saveBtn.setOnAction((ActionEvent saveSched) -> {
            progScreenController.saveSchedule(editSched);
        });
        Button runBtn = new Button("RUN");
        runBtn.setFont(new Font("ARIAL", 18));
        runBtn.setOnAction((ActionEvent enterOperation) -> {
            selectedDayIndex = 0;
            updateProgScreen();
            progScreenController.enterOperationMode();
        });
        resetBtn = new Button("RESET");
        resetBtn.setFont(new Font("ARIAL", 18));
        resetBtn.setOnAction((ActionEvent resetActiveSched) -> {
            editSched.resetSchedule(selectedDayIndex);
            updateProgScreen();
        });        
        
        confirmBtnBox.getChildren().addAll(saveBtn, runBtn, resetBtn);
        return confirmBtnBox;
    }
    
    private HBox loadHolidayBtns() {
        holidayBtns = new HBox();
        holidayBtns.setAlignment(Pos.CENTER);
        holidayBtns.setSpacing(8);
        
        addHolidayBtn = new Button("+HOLIDAY");
        addHolidayBtn.setOnAction((ActionEvent addHoliday) -> {
            editSched.addHoliday(holidayMonth, holidayDay, holidayYear);
            selectedDayIndex = 10 + editSched.getHolidayCount();
            updateProgScreen();
        });
        addHolidayBtn.setFont(new Font("ARIAL", 20));
        
        delHolidayBtn = new Button("-HOLIDAY");
        delHolidayBtn.setOnAction((ActionEvent delHoliday) -> {            
            editSched.delHoliday(selectedDayIndex - 10);
            selectedDayIndex = 10;
            updateProgScreen();
        });
        delHolidayBtn.setFont(new Font("ARIAL", 20));
        
        holidayBtns.getChildren().addAll(addHolidayBtn, delHolidayBtn);
        return holidayBtns;
    }
    
    private void showHolidayPicker(boolean newHoliday) {
        resetBtn.setVisible(false);
        if(newHoliday) {
            timeOfDay.setVisible(false);
            timePicker.setVisible(false);
            targetSettings.setVisible(false);
            modeSettings.setVisible(false);
        } else {
            timeOfDay.setVisible(true);
            timePicker.setVisible(true);
            targetSettings.setVisible(true);
            modeSettings.setVisible(true);
        }
        holidayPicker.setVisible(true);
        holidayBtns.setVisible(true);
        if(selectedDayIndex == 10) {
            addHolidayBtn.setVisible(true);
            delHolidayBtn.setVisible(false);
        }
        else if(selectedDayIndex > 10) {
            addHolidayBtn.setVisible(false);
            delHolidayBtn.setVisible(true);
        }
    }
    
    private void hideHolidayPicker() {
        resetBtn.setVisible(true);
        timeOfDay.setVisible(true);
        timePicker.setVisible(true);
        targetSettings.setVisible(true);
        modeSettings.setVisible(true);    
        holidayPicker.setVisible(false);
        holidayBtns.setVisible(false);
    }
}