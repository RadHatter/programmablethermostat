package com.programmablethermostat.main;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import javafx.stage.Stage;

/**
 *
 * @author aarjs
 * The thermostat controller bridges the gap between the GUI and the app's other
 * main components.
 */
public class ThermController implements ThermGUI.GuiController {
    
    private final ThermGUI thermGUI;
    private final NetHelper netHelper;
    private final DiskHelper diskHelper;
    
    //The thermstatus and thermschedule hold data to be viewed and displayed
    private ThermStatus thermStatus;
    private ThermSched activeSched;
    
    private final Gson GSON;
    
    public ThermController() {
        thermGUI = new ThermGUI();
        netHelper = new NetHelper();
        diskHelper = new DiskHelper();
        GSON = new GsonBuilder().setPrettyPrinting().create();   
    }
    
    public void startController(Stage stage) throws IOException, InterruptedException {
        //Checks for a reading from the associated online thermostat and converts it from JSON -> Java
        diskHelper.cacheTempReading(netHelper.GetTempReading());
        thermStatus = GSON.fromJson(diskHelper.loadTempReading(), ThermStatus.class);
        
        //Checks for a saved schedule file on local disk converts it from JSON -> Java
        String savedSched = diskHelper.loadSavedSched();
        if(savedSched != null) {
            activeSched = GSON.fromJson(savedSched, ThermSched.class);            
        }
        else {
            //Loads a fresh schedule if none exists
            activeSched = new ThermSched();
        }
        thermStatus.loadSchedParams(activeSched);
        thermGUI.LinkGuiController(this);
        thermGUI.setStage(stage);
        thermGUI.updateGUI(thermStatus, activeSched);
    }
    
    //Writes the programmed schedule to disk
    @Override
    public void saveSchedule(ThermSched saveSched) {
        activeSched = saveSched;
        diskHelper.cacheThermSched(GSON.toJson(activeSched));
    }
    
    //Toggles the active fan mode
    @Override
    public void toggleFan() {
        thermStatus.toggleFan();
        thermGUI.updateGUI(thermStatus, null);
    }
    
    //Toggles the active HVAC mode
    @Override
    public void toggleHvacMode() {
        thermStatus.toggleHvacMode();
        thermGUI.updateGUI(thermStatus, null);
    }
    
    //Toggles the active target temp up or down
    @Override
    public void changeTargetTemp(boolean targetUp) {
        thermStatus.changeTargetTemp(targetUp);
        thermGUI.updateGUI(thermStatus, null);
    }
}
