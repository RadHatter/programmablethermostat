package com.programmablethermostat.main;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * @author aarjs
 */
public class ProgrammableThermostat extends Application {    
    
    private static ThermController thermController;
    
    //Starts the thermostat's controller upon successful program launch
    @Override
    public void start(Stage stage) throws Exception {
        thermController = new ThermController();
        thermController.startController(stage);
    }
    
    public static void main(String[] args) {
        launch(args);
    }
}
