package com.programmablethermostat.main;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * @author aarjs
 */
public class NetHelper {
    
    //A single instance of the httpclient can handle all requests
    private final HttpClient thermClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2).build();
    
    //Returns the requested temperature reading as an object
    public String GetTempReading() throws IOException, InterruptedException {
        //Builds an appropriate HTTP GET request
        HttpRequest thermRequest = HttpRequest.newBuilder()
                .GET().uri(URI.create("http://media.capella.edu/BBCourse_Production/IT4774/temperature.json"))
                .setHeader("ThermUI", "0.1").build();
        
        //Caches the response received by the http client
        HttpResponse<String> tempReading = thermClient.send(thermRequest, HttpResponse.BodyHandlers.ofString());
        
        /*This line should not be necessary. The JSON is malformed and
        contains an extra comma after actualHumidity in the runtime block.
        Chromium-based browsers, the GSON library, and online JSON verifiers
        will be happy to confirm this. As such I'm using this quick-and-dirty
        fix because this would be an implementation fix for the server-side
        in the real world*/
        String fixedJSON = tempReading.body().replace(": 42,", ": 42");
        
        //Creates a new instance of the TempReading class from the response and returns it
        return fixedJSON;
    }
}


