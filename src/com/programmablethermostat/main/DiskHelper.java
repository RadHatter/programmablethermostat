package com.programmablethermostat.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author aarjs
 */
public class DiskHelper {
    
    public void cacheTempReading(String tempReadingJSON) {
        try {
            BufferedWriter diskWriter = new BufferedWriter(new FileWriter("tempReading.json"));
            diskWriter.write(tempReadingJSON);
            diskWriter.close();
        } catch(IOException e) {
            System.out.println("\nUnable to save temperature reading, try again later.\n");
        }
    }
    
    public String loadTempReading() throws IOException {
        File tempReadingFile = new File("tempReading.json");
        String tempReadingJSON = "";
        try {
            BufferedReader fileReader = new BufferedReader(new FileReader(tempReadingFile));
            String nextLine = "";
            while((nextLine = fileReader.readLine()) != null){
                tempReadingJSON += nextLine;
            }
        } catch (FileNotFoundException fnfE) {
            System.out.println("\nTemperature file not found, network may be unavailable!\n");            
        }  
        return tempReadingJSON;
    }
    
    public void cacheThermSched(String thermSchedJSON) {
                try {
            BufferedWriter diskWriter = new BufferedWriter(new FileWriter("thermSched.json"));
            diskWriter.write(thermSchedJSON);
            diskWriter.close();
        } catch(IOException e) {
            System.out.println(e.toString() + "\nUnable to save schedule, try again later.\n");
        }
    }
    
    public String loadSavedSched() throws IOException {
        File savedSchedFile = new File("thermSched.json");
        String savedSchedJSON = "";
        try {
            BufferedReader fileReader = new BufferedReader(new FileReader(savedSchedFile));
            String nextLine = "";
            while((nextLine = fileReader.readLine()) != null){
                savedSchedJSON += nextLine;
            }
        } catch (FileNotFoundException fnfE) {
            System.out.println("\nSchedule file not found. Has one been set?\n");
            return null;
        }  
        return savedSchedJSON;
    }
}


